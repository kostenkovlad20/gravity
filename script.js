$(document).ready(function () {
    $('.burger-menu').on('click', function () {
        $('.menu').slideToggle();
    });
    $('.close').on('click', function () {
        $('.menu').slideToggle();
    });
    $('.btn-play').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });
    var slider2 = new Swiper('.about-slider', {
        slidesPerView: 1,
        spaceBetween: 0,

        scrollbar: {
            el: '.slider-scrollbar',
            draggable: true,
            hide: false,
            snapOnRelease: false,
            dragSize: 0,
        },
        on: {
            init: function () {
                $('.js-current-slide').text(this.realIndex + 1);
                $('.js-all-slide').text(this.slides.length);
            },
            slideChange: function () {
                $('.js-current-slide').text(this.realIndex + 1);
            },
            setTranslate: function () {
                var progress = translateVal(this.scrollbar.dragEl);
                $('.slider-progress2').css('width', progress + 'px');
            },
            slideChangeTransitionEnd: function () {
                var progress = translateVal(this.scrollbar.dragEl);
                $('.slider-progress2').css('width', progress + 'px');
            }
        }
    });
    var swiper = new Swiper('.video-slider', {
        slidesPerView: 'auto',
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.btn-next',
            prevEl: '.btn-prev',
        },
    });
});


function translateVal(el) {
    var progress = el.style.transform.match(/translate3d\((.+)px,(.+)px,(.+)px\)/);
    return progress[1];
}

